#!/bin/sh

#Description: json parser written in shell

object_name="${1:?\"Please input an object name!\"}"
object_value=

rawdata=
one_line_data=

regexp_for_object='"[^:,{}"]*":\("[^"]*"\|[^,:]*\)'

#read data
while read rawdata
do
    rawdata="$(echo ${rawdata} | sed 's/: /:/g')" #clear space after :
    one_line_data="${one_line_data}${rawdata}"
done

#process
for object in $(echo ${one_line_data} | grep -o "${regexp_for_object}")
do
    case $(echo ${object} | grep -o ':.*' | cut -d ':' -f 2) in
	\{*)
	    printf "%s is an object.\n" "$(echo "${object}" | grep -o '".*":' | grep -o '[^":]*')"
	    exit 1
	    ;;
	\[*)
	    printf "%s is an array.\n" "$(echo "${object}" | grep -o '".*":' | grep -o '[^":]*')"
	    exit 1
	    ;;
    esac

    if [ "$(echo "${object}" | grep -o '".*":' | grep -o '[^":]*')" = "${object_name}" ]
    then
	object_value=$(echo "${object}" | grep -o ':.*' | cut -d ':' -f 2-)
	break
    fi
done

#print
    printf "%s\n" "${object_value}"

#end here!!hhh
